﻿using E_Commerce.Entities;
using E_Commerce.IServices;
using E_Commerce.Pages;

namespace E_Commerce.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork _unitOfWork;
        public CustomerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddCustomer(Customer customer)
        {
            await _unitOfWork.CustomerRepository.AddAsync(customer);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                Console.WriteLine("Add Succeed");
            }
        }

        public async Task<bool> DeleteCustomer(int id)
        {
            var customer = await _unitOfWork.CustomerRepository.GetById(id);
            if (customer is null)
            {
                Console.WriteLine($"Not found product ID {id}");
                return false;
            }
            _unitOfWork.CustomerRepository.Delete(customer);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                Console.WriteLine("Delete Succeed");
            }

            return true;
        }

        public async Task<List<Customer>?> GetAllCustomer() => await _unitOfWork.CustomerRepository.GetAllAsync();

        public async Task<Customer> GetCustomerById(int id) => await _unitOfWork.CustomerRepository.GetById(id);

        public async Task UpdateCustomer(Customer customer)
        {
            _unitOfWork.CustomerRepository.Update(customer);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                Console.WriteLine("Update Succeed");
            }
        }
    }
}
