﻿using E_Commerce.Entities;

namespace E_Commerce.IRepository
{
    public interface IProductRepository: IGenericRepository<Product>
    {
    }
}
