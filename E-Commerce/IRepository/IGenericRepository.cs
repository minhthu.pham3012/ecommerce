﻿namespace E_Commerce.IRepository
{
    public interface IGenericRepository<Entity>
    {
        Task AddAsync(Entity entity);
        void Update(Entity entity);
        void Delete(Entity entity);
        Task<Entity?> GetById(int id);
        Task<List<Entity>?> GetAllAsync();
        
    }
}
