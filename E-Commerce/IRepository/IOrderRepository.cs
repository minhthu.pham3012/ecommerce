﻿using E_Commerce.Entities;

namespace E_Commerce.IRepository
{
    public interface IOrderRepository: IGenericRepository<Order>
    {
        Task<List<Order>?> GetOrderByCustomerId(int id);
    }
}
