﻿using E_Commerce.Entities;
using E_Commerce.IServices;
using E_Commerce.Services;
using Microsoft.AspNetCore.Mvc;

namespace E_Commerce.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        public async Task<IActionResult> GetCustomer()
        {
            var a = await _customerService.GetAllCustomer();
            return Ok(a);
        }

        [HttpPost]
        public async Task<IActionResult> AddCustomer(Customer customer)
        {
            await _customerService.AddCustomer(customer);
            return Ok(customer);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomer(int id)
        {
            await _customerService.DeleteCustomer(id);
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCustomer([FromRoute] int id, [FromBody] Customer customer)
        {
            var a = await _customerService.GetCustomerById(id);
            if (a is not null)
            {
                a.FullName = customer.FullName;
                a.Address = customer.Address;
                a.Address = customer.Address;
                await _customerService.UpdateCustomer(a);
                return Ok(customer);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
