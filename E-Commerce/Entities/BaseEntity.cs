﻿namespace E_Commerce.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
        public bool isDeleted { get; set; }
    }
}
