﻿namespace E_Commerce.Entities
{
    public class Customer: BaseEntity
    {
        public string FullName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public ICollection<Order>? Orders { get; set; }
    }
}
