﻿namespace E_Commerce.Entities
{
    public class Order: BaseEntity
    {
        public DateTime OrderDate { get; set; }
        public decimal Total { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        public ICollection<OrderItems>? OrderItems { get; set; }
    }
}
