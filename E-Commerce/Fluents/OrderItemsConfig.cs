﻿using E_Commerce.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace E_Commerce.Fluents
{
    public class OrderItemsConfig: IEntityTypeConfiguration<OrderItems>
    {
        public void Configure(EntityTypeBuilder<OrderItems> builder)
        {
            builder.HasOne(x => x.Product)
                  .WithMany(x => x.OrderItems)
                  .HasForeignKey(x => x.ProductId);

            builder.HasOne(x => x.Order)
                   .WithMany(x => x.OrderItems)
                   .HasForeignKey(x => x.OrderId);
        }
    }
}
