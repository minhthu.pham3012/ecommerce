﻿using E_Commerce.IRepository;

namespace E_Commerce
{
    public interface IUnitOfWork
    {
        ICustomerRepository CustomerRepository { get; }
        IProductRepository ProductRepository { get; }
        IOrderRepository OrderRepository { get; }
        IOrderItemsRepository OrderItemsRepository { get; }
        Task<int> SaveChangeAsync();

    }
}
