﻿using E_Commerce.IRepository;
using E_Commerce.Repository;
using Microsoft.EntityFrameworkCore;

namespace E_Commerce
{
    public class UnitOfwork : IUnitOfWork
    {
        public readonly AppDbContext _context;
        private readonly IProductRepository _productRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly ICustomerRepository _customerRepository; 
        private readonly IOrderItemsRepository _orderItemsRepository;

        public UnitOfwork(AppDbContext context, IProductRepository productRepository, ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderItemsRepository orderItemsRepository)
        {
            _context = context;
            _productRepository = productRepository;
            _customerRepository = customerRepository;
            _orderRepository = orderRepository; 
            _orderItemsRepository = orderItemsRepository;

        }
        public IProductRepository ProductRepository => _productRepository;
        public IOrderRepository OrderRepository => _orderRepository;
        public IOrderItemsRepository OrderItemsRepository => _orderItemsRepository; 
        public ICustomerRepository CustomerRepository => _customerRepository;

       

        public async Task<int> SaveChangeAsync()
        {
            var a = await _context.SaveChangesAsync();
            return a;
        }
    }
}
