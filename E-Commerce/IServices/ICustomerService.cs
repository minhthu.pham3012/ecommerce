﻿using E_Commerce.Entities;

namespace E_Commerce.IServices
{
    public interface ICustomerService
    {
        Task AddCustomer(Customer customer);
        Task<bool> DeleteCustomer(int id);
        Task UpdateCustomer(Customer customer);
        Task<Customer> GetCustomerById(int id);
        Task<List<Customer>?> GetAllCustomer();
    }
}
