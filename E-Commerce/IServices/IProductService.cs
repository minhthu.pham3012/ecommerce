﻿using E_Commerce.Entities;

namespace E_Commerce.IServices
{
    public interface IProductService
    {
        Task AddProduct(Product product);
        Task DeleteProduct(Product product);
        Task UpdateProduct(Product product);
        Task<Product?> GetProductById(int id);
        Task<List<Product>> GetProduct();
    }
}
