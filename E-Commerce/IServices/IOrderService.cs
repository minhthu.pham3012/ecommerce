﻿using E_Commerce.Entities;

namespace E_Commerce.IServices
{
    public interface IOrderService
    {
        Task AddOrder(Order order);
        Task DeleteOrder(int id);
        Task UpdateOrder(Order order);
        Task<Order?> GetOrder(int id);
        Task<Order?> GetOrderByCustomerId(int id);
    }
}
