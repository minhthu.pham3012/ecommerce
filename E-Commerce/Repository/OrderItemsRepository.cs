﻿using E_Commerce.Entities;
using E_Commerce.IRepository;

namespace E_Commerce.Repository
{
    public class OrderItemsRepository : GenericRepository<OrderItems>, IOrderItemsRepository
    {
        public OrderItemsRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
