﻿using E_Commerce.Entities;
using E_Commerce.IRepository;

namespace E_Commerce.Repository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
