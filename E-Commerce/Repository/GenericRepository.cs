﻿using E_Commerce.Entities;
using E_Commerce.IRepository;
using Microsoft.EntityFrameworkCore;

namespace E_Commerce.Repository
{
    public class GenericRepository<Entity> : IGenericRepository<Entity> where Entity : BaseEntity
    {
        protected DbSet<Entity> _dbSet;
        public GenericRepository(AppDbContext dbContext)
        {
            _dbSet = dbContext.Set<Entity>();
        }
        public async Task AddAsync(Entity entity)
        {
            await _dbSet.AddAsync(entity);
        }

        public async Task<List<Entity>?> GetAllAsync() => await _dbSet.AsNoTracking().ToListAsync();

        public Task<Entity?> GetById(int id) => _dbSet.FirstOrDefaultAsync(x => x.Id == id);


        public void Delete(Entity entity)
        {
            entity.isDeleted = true;
        }

        public void Update(Entity entity)
        {
            _dbSet.Update(entity);
        }
    }
}
