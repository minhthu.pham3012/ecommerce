﻿using E_Commerce.Entities;
using E_Commerce.IRepository;

namespace E_Commerce.Repository
{
    public class CustomerRepository: GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
