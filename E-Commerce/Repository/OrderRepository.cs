﻿using E_Commerce.Entities;
using E_Commerce.IRepository;

namespace E_Commerce.Repository
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(AppDbContext dbContext) : base(dbContext)
        {
        }

        public Task<List<Order>?> GetOrderByCustomerId(int id)
        {
            throw new NotImplementedException();
        }
    }
}
